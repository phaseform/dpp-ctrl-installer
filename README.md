## Installation package for DPP controlling

This repository includes the installation instructions and file with ***whl*** extension for installing the controlling package.   
The installation file is provided in the "wheel" Python format: **dpp_ctrl-... .whl**.   

### Setup instructions

#### Prerequisites
1) The installed Python interpreter (recommended versions 3.9.. and 3.10..) + ***pip*** - the default package management tool.    
   1.1 Note that for Python version > 3.4, ***pip*** could be installed along with the Python standard installation package.       
2) Internet connection for downloading all Python dependencies (required for command **pip install**).     

#### Getting the installation package (archive or *whl file)   
For downloading it directly: click on the file; after click the icon / string "Download".     

#### Basic installation   
For installation of this controlling package, run in the command prompt (i.e., command line / console) the following command:         
**pip install path_to_wheel** , where **path_to_wheel** - absolute path to the location with the downloaded wheel archive.   
Hint: it's easier to navigate in the command prompt (console) to the download directory and run command from it.

#### Launching / using of the GUI-based controlling program
1) Launch of the graphical user interface (GUI) of the controlling program as the standalone program:     
**python**  - for launching the Python console, all following commands should be run within it:     
**from dpp_ctrl import gui_dpp** - if it's successful, the new line in the console will be returned.        
**gui_dpp.launch()** - launches the graphical user interface (GUI) of the controlling program in the Simulation mode (see the User Manual).
Note: if the python is not launchable from the command prompt, then launch it as the standalone program.    

2) Launch the controlling program with the attempt of automatic initialization of connection to the DPP device: 
First, launch the Python console and import the *gui_dpp_ctrl* module as it's described above in the 1st subpart.   
Second, run the following command: **gui_dpp.external_launch()**    

#### Using API functions for sending aberrations to the device    
1) Import the script in python console: **from dpp_ctrl import api_dpp**      
2) Controlling class initialization and assigning to the variable: **pydppctrl = api_dpp.initialize()**     
3) Basic example of the connection initialization and device control:     
**pydppctrl.connect_device(port_name="Name Port")** - connecting to the device connected to the serial port with string "Name Port";     
**pydppctrl.load_precalibration()** - attempting to load calibration files from the installation location (path_to_installed_package\calibrations\);     
or **pydppctrl.load_infl_matrix()** - for using the system file dialog window for navigating to the calibration file location;       
the same for loading flat field corrections: **pydppctrl.load_flat_field()**;    
Note that the last method call is only needed if the influence matrix loaded not from the *json precalibration file.   
4) To send aberrations to the device: **pydppctrl.apply_phases(phases)**, where    
*phases* - either dictionary like {1: 0.1} or {(1, 1): 0.1} - amplitudes with OSA integer index or tuple with azimuthal, radial orders as dictionary keys and an amplitude - as a dictionary value,      
or list with amplitudes with OSA indexing scheme for Zernike polynomials, like [0.0, 0.1, 0.1, 0.0, -0.2].    
5) In the end of the script, call the releasing system resources (closing) function:  **pydppctrl.close()**

### Update instructions   
Download the updated version of a wheel file (**.whl**) from this repository and run the following command in the command prompt:   
**pip install path_to_the_wheel**      
Hint: **pip show dpp_ctrl** - gives short description of the installed controlling package and its version.

### *Appendix*

#### About the Python wheel format
The information  about the wheel format: https://peps.python.org/pep-0491/

#### Methods of the controlling class from the *api_dpp* module   
They are listed in the separate file as the API dictionary, provided along with the updated User Manual.    

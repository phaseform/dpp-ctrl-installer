#!/usr/bin/env python3

from multiprocessing import Queue
from dpp_ctrl import gui_dpp

mpQueue = Queue()
ctrl_dpp_prc = gui_dpp.IndPrcLauncher(mpQueue)
ctrl_dpp_prc.start()
phase_dictionary = {"(-3,3)": 0.5, "(-2, 2)": -0.5}
mpQueue.put_nowait(phase_dictionary)

mpQueue.put_nowait("EXIT")

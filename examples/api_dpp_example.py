#!/usr/bin/env python3
from dpp_ctrl import api_dpp
import time
from pathlib import Path

# Configure COM port used for serial communication
port = "/dev/ttyUSB0" # Windows: "COM4"

# Absolute path to calibration file
cal_file = Path("~/.virtualenvs/dpp_ctrl/lib/python3.10/"\
                        "site-packages/dpp_ctrl/"\
                        "calibrations/Calibration.json").expanduser()

# Limit electronics Driver maximum
VMAX = 300


def init():
    # initiliaze
    dpp = api_dpp.initialize()
    opened_flag = False

    if dpp.connect_device(port_name=port):

        dpp.set_max_voltage(VMAX)   # default is also 300
        dpp.zero_outputs()  # zero all outputs to 0V

        # load calibration
        # no argument launches file chooser
        if dpp.load_infl_matrix(str(cal_file)):
            opened_flag = True
    else:
        print("Could not connect!")

    return dpp, opened_flag


def main():
    # run intialization and load calibration
    dpp, opened = init()

    if opened:

        # set amplitude of target modes
        phases = {(2, 2): 0.5,
                  (-2, 2): -0.5}  # m,n: amplitude
        dpp.apply_phases(phases)

        time.sleep(5)  # 5 sec delay

        # set amplitude of target modes
        phases = {(2, 0): 0.5,
                  (-2, 4): -0.5}
        dpp.apply_phases(phases)

        # approximated shape of DPP wrt. target
        print(dpp.get_approx_ampls())

        time.sleep(5)

        # test DELTA7. Loop through each channel
        dpp.zero_outputs()  # zero all outputs to 0V
        dpp.launch_test_loop()

        time.sleep(5)

    dpp.close()


# Run this script
if __name__ == "__main__":
    main()
